# Otus_task1

```
#01/08/2019
Пункт 0 - добавил виртуалке второе ядро и еще гиг памяти

#Первым делом я обновил все пакеты
yum update
#Затем поставил нужные пакеты для сборки
yum install -y ncurses-devel make gcc bc bison flex elfutils-libelf-devel openssl-devel grub2 wget mc vim
cd /usr/src/
#Скачал самое свежее ядро
wget https://cdn.kernel.org/pub/linux/kernel/v5.x/linux-5.2.5.tar.xz
#Распаковал
tar -xvf linux-5.2.5.tar.xz

#Придумал себе задачу скомпилировать новое и быстрое ядро, чтобы система грузилась быстрее(внизу приведены примеры замеров времени загрузки)
#Запускаю конфигурацию ядра
make menuconfig
Delete -
SoundCard support
Microsoft Hyper-V guest support
nfs gfs2 ocfs2 btrfs support
#Удалил не нужную мне звуковую карту и поддержку нескольких файловых систем


#Собираю
make -j3 bzImage
make -j3 modules
make -j3
#Устанавливаю
make modiles_install 
make install


#Редактирую загрузчик
#чтобы моя первая запись грузилась по умолчанию
vim /etc/default/grub
...
GRUB_DEFAULT=0
...
#Записываю загрузчик
grub2-mkconfig -o /boot/grub2/grub.cfg
#ребут
restart -f
#Получилось с первого раза, не ожидал)

#Примеры скорости загрузки

#systemd-analyze  Startup finished in
#556ms (kernel) + 1.006s (initrd) + 4.335s (userspace) = 5.898s
#556ms (kernel) + 1.310s (initrd) + 7.219s (userspace) = 9.086s
#545ms (kernel) + 728ms (initrd) + 7.346s (userspace) = 8.620s
#525ms (kernel) + 691ms (initrd) + 6.212s (userspace) = 7.429s


#second machine
#583ms (kernel) + 1.166s (initrd) + 7.633s (userspace) = 9.384s
#525ms (kernel) + 942ms (initrd) + 7.473s (userspace) = 8.941s
#517ms (kernel) + 975ms (initrd) + 6.734s (userspace) = 8.227s
#684ms (kernel) + 1.333s (initrd) + 9.273s (userspace) = 11.291s
#566ms (kernel) + 1.011s (initrd) + 5.129s (userspace) = 6.707s
#546ms (kernel) + 1.035s (initrd) + 7.939s (userspace) = 9.521s


#with 5.2.5 kernel
#1.535s (kernel) + 1.233s (initrd) + 3.423s (userspace) = 6.192s
#1.526s (kernel) + 1.105s (initrd) + 3.724s (userspace) = 6.357s
#1.521s (kernel) + 1.170s (initrd) + 3.718s (userspace) = 6.409s
#1.203s (kernel) + 886ms (initrd) + 2.861s (userspace) = 4.951s
#1.078s (kernel) + 941ms (initrd) + 2.730s (userspace) = 4.749s
#1.253s (kernel) + 990ms (initrd) + 3.500s (userspace) = 5.744s

#время загрузки ядра увеличилось, а userpace уменьшилось, в итоге времени занимает меньше по цифрам
```